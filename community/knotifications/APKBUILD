# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knotifications
pkgver=5.91.0
pkgrel=0
pkgdesc="Abstraction for system notifications"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="BSD-3-Clause AND LGPL-2.0-or-later AND LGPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kwindowsystem-dev
	libcanberra-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtspeech-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knotifications-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring running dbus-daemon

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
e5d2bfe468e080080c754f632e75d85999d487071b73ecfb511ea26ad6a1dfd0b333f6fededab82a53a70fbeb33c05c5623ae286670408544134e591bc01cc96  knotifications-5.91.0.tar.xz
"
