# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kimageformats
pkgver=5.91.0
pkgrel=1
pkgdesc="Image format plugins for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	libavif-dev
	openexr-dev
	qt5-qtbase-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kimageformats-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kimageformats-read-psd"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
f22ea4e35b169fd41bc9f7bf58362597b99190390c80f9ff3350c7ba6590e609b88efd2d6c21c43b33336c4f55edd4f811828706683f48f04a5c75bedc4fcb2d  kimageformats-5.91.0.tar.xz
"
