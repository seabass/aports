# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=certbot
pkgver=1.24.0
pkgrel=0
pkgdesc="An ACME client that can update Apache/Nginx configurations"
url="https://github.com/certbot/certbot"
arch="noarch"
license="Apache-2.0"
depends="
	py3-acme
	py3-configargparse
	py3-configobj
	py3-cryptography
	py3-distro
	py3-distutils-extra
	py3-josepy
	py3-parsedatetime
	py3-pyrfc3339
	py3-tz
	py3-setuptools
	py3-zope-component
	py3-zope-interface
"
checkdepends="py3-augeas py3-pytest"
replaces="letsencrypt"
source="https://pypi.io/packages/source/c/certbot/certbot-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
bea59ba44156066a3e6c946abe24fb3a89c043471683c68f0846042fcd31e1e898a924e530f713a383064f7296f932e336c1fa5d9ecb0766e34a891dcde52a92  certbot-1.24.0.tar.gz
"
