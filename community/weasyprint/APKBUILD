# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=weasyprint
pkgver=54.1
pkgrel=0
pkgdesc="A visual rendering engine for HTML and CSS that can export to PDF"
url="https://weasyprint.org"
arch="noarch"
license="BSD-3-Clause"
# py3-cairocffi 1.2.0-r3 is broken, at least on aarch64
depends="
	pango
	py3-cssselect2
	py3-cffi
	py3-fonttools
	py3-html5lib
	py3-pillow
	py3-pydyf
	py3-pyphen
	py3-tinycss2
	"
makedepends="
	py3-build
	py3-flit
	py3-pip
	"
checkdepends="
	ghostscript
	py3-pytest
	py3-pytest-cov
	py3-pytest-flake8
	py3-pytest-isort
	ttf-dejavu
	"
replaces="py-weasyprint py3-weasyprint"  # for backward compatibility
provides="py-weasyprint=$pkgver-r$pkgrel py3-weasyprint=$pkgver-r$pkgrel"  # for backward compatibility
source="https://files.pythonhosted.org/packages/source/w/weasyprint/weasyprint-$pkgver.tar.gz"

case "$CARCH" in
	# Many tests fail
	s390x) options="!check";;
esac

build() {
	python3 -m build --skip-dependency-check --no-isolation --wheel .
}

check() {
	pytest
}

package() {
	python3 -m pip install --root="$pkgdir" \
		--ignore-installed --no-deps --no-warn-script-location \
		"$builddir"/dist/weasyprint-$pkgver-py3-none-any.whl
}

sha512sums="
618bdb9872a37185ac687af33ac0e207cf121ad318a62111a9cb5205c3487df036db5b370def1a65bed0de4ac1ddb42a87f658d1ff741fe7e7a6733ddfdfaf03  weasyprint-54.1.tar.gz
"
