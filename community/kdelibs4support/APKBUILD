# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdelibs4support
pkgver=5.91.0
pkgrel=0
pkgdesc="Porting aid from KDELibs4"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND MIT AND LGPL-2.1-only AND LGPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only) AND (LGPL-2.0-only OR LGPL-3.0-only)"
depends_dev="
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kded-dev
	kdesignerplugin
	kdesignerplugin-dev
	kdoctools-dev
	kemoticons-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kitemmodels-dev
	knotifications-dev
	kparts-dev
	kservice-dev
	ktextwidgets-dev
	kunitconversion-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	perl-uri
	qt5-qtbase-dev
	qt5-qttools-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kded
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdelibs4support-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="suid !check" # Fails due to requiring running dbus

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}


package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
1407e3fc9f076f7e4621c2cc05ad861f88127998169fbad874e6c575c6820b900d1e7b6fd552ff8a45fb8a9b84d6b6c77486f98162f7dd6fab5738c0ee5932e8  kdelibs4support-5.91.0.tar.xz
"
