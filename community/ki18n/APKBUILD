# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ki18n
pkgver=5.91.0
pkgrel=0
pkgdesc="Advanced internationalization framework"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-or-later)"
depends_dev="
	qt5-qtdeclarative-dev
	qt5-qtscript-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	doxygen
	graphviz
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ki18n-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# ki18n-klocalizedstringtest, kcountrytest and kcountrysubdivisiontest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(ki18n-klocalizedstring|kcountry|kcountrysubdivision)test"
}


package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6bb7a67589ea19a39de628b412f1e57e3717d7f9640405d513ebc5edccb091dd7dddbc75db806274e2302466428a2d256a8ec00613a285ba9fdc5ba2bb454e66  ki18n-5.91.0.tar.xz
"
