# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kded
pkgver=5.91.0
pkgrel=0
pkgdesc="Extensible deamon for providing system level services"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kservice-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kded-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}


package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}
sha512sums="
1f42b35f577102bc9f74b678c7892569eb3eef653261d66181b6f15a0d9c9fef09850969da1047a5faafe4234f514fc024595a6d8f72c9a26d33a554e20b5cef  kded-5.91.0.tar.xz
"
