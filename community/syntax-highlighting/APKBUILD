# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=syntax-highlighting
pkgver=5.91.0
pkgrel=0
pkgdesc="Syntax highlighting engine for structured text and code"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="MIT"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/syntax-highlighting-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
9d891f4a40df757d432639fce86374661a088a7b488f2f4515db783cca727b49bec65f558f5b8892b2149f8f9d7dcab7172d10766dadd959eadc4fcad9f50c39  syntax-highlighting-5.91.0.tar.xz
"
