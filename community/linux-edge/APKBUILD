# Maintainer: Milan P. Stanić <mps@arvanta.net>

_flavor=edge
pkgname=linux-${_flavor}
# NOTE: this kernel is intended for testing
# please resist urge to upgrade it blindly
pkgver=5.16.12
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=0
pkgdesc="Linux latest stable kernel"
url="https://www.kernel.org"
depends="mkinitfs"
_depends_dev="perl gmp-dev elfutils-dev bash flex bison"
makedepends="$_depends_dev sed installkernel bc linux-headers linux-firmware-any
	openssl1.1-compat-dev diffutils findutils xz"
options="!strip !check" # no tests
_config=${config:-config-edge.${CARCH}}
install=

subpackages="$pkgname-dev:_dev:$CBUILD_ARCH"
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz"
case $pkgver in
	*.*.0)	source="$source";;
	*.*.*)	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz" ;;
esac

	source="$source
	config-edge.aarch64
	config-edge.armv7
	config-edge.x86_64
	config-edge.riscv64
	config-edge4virt.aarch64
	config-edge4virt.armv7
	config-edge4virt.x86_64
	config-edge4virt.riscv64
	"

arch="armv7 aarch64 x86_64 riscv64"
license="GPL-2.0"

_flavors=
for _i in $source; do
	case $_i in
	config-*.$CARCH)
		_f=${_i%.$CARCH}
		_f=${_f#config-}
		_flavors="$_flavors ${_f}"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-${_f}::$CBUILD_ARCH linux-${_f}-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done

_carch=${CARCH}
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
riscv64) _carch="riscv" ;;
esac

prepare() {
	local _patch_failed=
	cd "$srcdir"/linux-$_kernver
	case $pkgver in
		*.*.0);;
		*)
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N ;;
	esac

	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch)
			msg "Applying $i..."
			if ! patch -s -p1 -N -i "$srcdir"/$i; then
				echo $i >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	# remove localversion from patch if any
	rm -f localversion*
	oldconfig
}

oldconfig() {
	for i in $_flavors; do
		local _config=config-$i.${CARCH}
		local _builddir="$srcdir"/build-$i.$CARCH
		mkdir -p "$_builddir"
		echo "-$pkgrel-$i" > "$_builddir"/localversion-alpine \
			|| return 1

		cp "$srcdir"/$_config "$_builddir"/.config
		make -C "$srcdir"/linux-$_kernver \
			O="$_builddir" \
			ARCH="$_carch" \
			listnewconfig oldconfig
	done
}

build() {
	unset LDFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		cd "$srcdir"/build-$i.$CARCH
		make ARCH="$_carch" CC="${CC:-gcc}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	local _abi_release=${pkgver}-${pkgrel}-${_buildflavor}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$srcdir"/build-$_buildflavor.$CARCH
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		riscv64) _install="install dtbs_install";;
		*) _install=install;;
	esac

	make -j1 modules_install $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/${_abi_release}/build \
		"$_outdir"/lib/modules/${_abi_release}/source
	rm -rf "$_outdir"/lib/firmware

	install -D -m644 include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package edge "$pkgdir"
}

# subflavors install in $subpkgdir
edge4virt() {
	_package edge4virt "$subpkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _abi_release=${pkgver}-${pkgrel}-$_flavor
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-${_abi_release}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/config-$_flavor.${CARCH} "$dir"/.config
	echo "-$pkgrel-$_flavor" > "$dir"/localversion-alpine

	make -j1 -C "$srcdir"/linux-$_kernver O="$dir" ARCH="$_carch" \
		syncconfig prepare modules_prepare scripts

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the soruces
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk
	cd "$srcdir"/linux-$_kernver
	find .  -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' -o -name 'Platform' \) \
		-print | cpio -pdm "$dir"

	cp -a scripts include "$dir"

	find $(find arch -name include -type d -print) -type f \
		| cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build-$_flavor.$CARCH/Module.symvers \
		"$dir"/Module.symvers

	mkdir -p "$subpkgdir"/lib/modules/${_abi_release}
	ln -sf /usr/src/linux-headers-${_abi_release} \
		"$subpkgdir"/lib/modules/${_abi_release}/build
}

sha512sums="
7a257dd576bc8493595ec7d6f3c9cb6e22c772a8b2dbe735d2485c4f5c56e26a08695546e7e0f1f1cd04a533f25e829361958d4da0b98bf0ba8094dd57a85aaf  linux-5.16.tar.xz
71ce07e8915d3bbae0f43331ee001d3e6df1751380ffca7b939e5132101b25bf3f3224d32a455798a611d61641e579759b37e0e4b831f126ba9ee90cc8c829ef  patch-5.16.12.xz
1176a59b9a03a8987030f1b4fe42aa413803b3b2b90642855ee926c2c0ddcca4f829ba3bf4cbd08e59a63f56853964cd732ffdd86f7d5ca40bdafe581f21316a  config-edge.aarch64
7e0669a4be68e154f1341ff335798df1f6f7632fc6f9b8b77ea2b4900efc0cd104efd91fb0ab6d0df0210f88cc4ee734e5ff8209547c7c17f6ec6fd4efb0bba8  config-edge.armv7
1fcde8d57cbf1d9e81a0535cc2a9d27a6676db422314568812cfcc67df5d3ed1d16d0cece7faead463dd9cd4cc3293622825a5adaa654570c2d52038141f3cd6  config-edge.x86_64
2c582232fb0d0b53bd08495bc8ad8ffd86b84f153f870b8846d78dedbbd28a3663b20b631297e46da24578ae34498547ad9fb1f9a829c974ec9d5fd7d57f5a42  config-edge.riscv64
eaa6b69b795fe8c09d1fd380d39b0f245cbc01a426d9af7fbedcecf1262447220f4e4730d34e5c8c39fecc6faacda02659112d239a85dcc60cd67a0a60490d46  config-edge4virt.aarch64
e70e078903fc72aa15eda205148f8db24b5a33dad79b58f750e6ee8bd253e002d256753fc4ece9919e10529e31f4b4890df4f83395e45b6252c2d16950830a6a  config-edge4virt.armv7
b4349cfadf6e132214812ffe874779768fc7778d49ac8c4bd2a3542ba62ff58121baa4c6a50b0878601f7cc351edff13858acd2a37a23c92d2e5ac834da60f7a  config-edge4virt.x86_64
4111273276456ebbd3ac79b796cc520dbb61fe808c0172ceefdc9e178dd9a7ce72c5acd6af43880313f7a498c741bb82ed396a6f4e037526bdc0c589061d9ce0  config-edge4virt.riscv64
"
