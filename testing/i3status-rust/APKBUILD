# Contributor: Galen Abell <galen@galenabell.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=i3status-rust
pkgver=0.21.7
pkgrel=0
pkgdesc="i3status replacement in Rust"
url="https://github.com/greshake/i3status-rust"
arch="all !s390x !riscv64" # limited by cargo
license="GPL-3.0-only"
makedepends="rust cargo dbus-dev openssl-dev libpulse lm-sensors-dev"
options="net !check" # no tests
provides="i3status-rs"
subpackages="$pkgname-doc"
source="https://github.com/greshake/i3status-rust/archive/v$pkgver/i3status-rust-$pkgver.tar.gz"

build() {
	cargo build --release --verbose --locked
}

package() {
	install -Dm755 target/release/i3status-rs "$pkgdir"/usr/bin/i3status-rs

	install -Dm644 man/i3status-rs.1 "$pkgdir"/usr/share/man/man1/i3status-rs.1

	install -Dm644 -t "$pkgdir"/usr/share/i3status-rust/themes files/themes/*
	install -Dm644 -t "$pkgdir"/usr/share/i3status-rust/icons files/icons/*
}

sha512sums="
90bab2c2abd8adc55fe6e1c99f4660171b08d08576164d9cb2df9a5db143bc7345012193c6a2e0dab87f96bdcd2d344a59a50f3b3e14d6db834fe68ca9b1bbbd  i3status-rust-0.21.7.tar.gz
"
