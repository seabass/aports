# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=miniserve
pkgver=0.19.2
pkgrel=0
pkgdesc="Quickly serve files via HTTP"
url="https://github.com/svenstaro/miniserve"
license="MIT"
arch="all !s390x !riscv64" # limited by rust/cargo
arch="$arch !ppc64le" # FTBFS
makedepends="cargo"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/svenstaro/miniserve/archive/v$pkgver/miniserve-$pkgver.tar.gz"

build() {
	cargo build --release --locked

	./target/release/miniserve --print-manpage > $pkgname.1

	./target/release/miniserve --print-completions bash > $pkgname.bash
	./target/release/miniserve --print-completions fish > $pkgname.fish
	./target/release/miniserve --print-completions zsh > $pkgname.zsh
}

check() {
	cargo test --release --locked
}

package() {
	install -Dm755 target/release/miniserve "$pkgdir"/usr/bin/miniserve

	install -Dm644 $pkgname.1 "$pkgdir"/usr/share/man/man1/$pkgname.1

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
7afc9d60a9931e571a0b83fba168b87546a6eedc924465eceb9004e3416286165f9ad1ad658bc44dc419e4b83a4fd6193eeba60732b48f89b1a801ca0d132ebf  miniserve-0.19.2.tar.gz
"
