# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=grype
pkgver=0.33.1
pkgrel=0
pkgdesc="Vulnerability scanner for container images, filesystems, and SBOMs"
url="https://github.com/anchore/grype"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # FTBFS on 32-bit arches
makedepends="go"
source="https://github.com/anchore/grype/archive/v$pkgver/grype-$pkgver.tar.gz"
options="!check" # tests need docker

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -o bin/grype
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/grype "$pkgdir"/usr/bin/grype
}

sha512sums="
57b4983cefb7a50d27ccbb8310906c0d3fcd2a595305ed9035957be44af848642fb36723262bcf0de2c04e9efb74f7cdd83935598c5ef7ad8992929b2cd51f5b  grype-0.33.1.tar.gz
"
